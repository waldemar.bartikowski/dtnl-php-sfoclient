<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Tests\TestClass;

use DTNL\SfoClient\Entity\AbstractSfoEntity;

class ConcreteSfoEntity extends AbstractSfoEntity {}