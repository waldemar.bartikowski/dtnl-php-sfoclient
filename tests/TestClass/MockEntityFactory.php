<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Tests\TestClass;

use DTNL\SfoClient\Tests\TestClass\ConcreteSfoEntity;
use DTNL\SfoClient\Entity\Interfaces\SfoEntityInterface;
use DTNL\SfoClient\Entity\Interfaces\SfoEntityFactoryInterface;
use DTNL\SfoClient\Entity\AbstractSfoEntity;

class MockEntityFactory implements SfoEntityFactoryInterface {

    public function createEntity( $entry ) : SfoEntityInterface {
        return new ConcreteSfoEntity();
    }

}