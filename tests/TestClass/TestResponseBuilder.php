<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Tests\TestClass;

use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Psr7\Response;

class TestResponseBuilder {

    public static function createJobRequisionXml() : ResponseInterface {

        $headers = [ 'Content-Type' => 'application/atom+xml;charset=utf-8' ];
        $body = file_get_contents( __DIR__ . '/../TestData/jobrequisition.xml' );
        
        if ( $body === false ) {
            throw new \RuntimeException(
                'Not able to lead Job Requision XML data.'
            );
        }
        return new Response( 200, $headers, $body );
    }
}