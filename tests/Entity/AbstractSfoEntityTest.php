<?php

declare( strict_types = 1 );
namespace DTNL\SfoClient\Tests\Entity;

use \PHPUnit\Framework\TestCase;
use DTNL\SfoClient\Tests\TestClass\ConcreteSfoEntity;
use DTNL\SfoClient\Entity\AbstractSfoEntity;

/**
 * @covers DTNL\SfoClient\Entity\AbstractSfoEntity
 */
class AbstractSfoEntityTest extends TestCase {

    public function testAddingProperty() : void {
        $entity = new ConcreteSfoEntity();
        $entity
            ->setProperty( 'first', 'first value' )
            ->setProperty( 'second', 'second value' )
        ;

        self::assertEquals( 'first value', $entity['first'] );
        self::assertEquals( 'second value', $entity['second'] );
    }

    public function testRemovingProperty() : void {
        $entity = new ConcreteSfoEntity();
        $entity
            ->setProperty( 'first', 'first value' )
            ->setProperty( 'second', 'second value' )
        ;
        
        $entity->removeProperty( 'first' );

        self::assertFalse( $entity->hasProperty( 'first') );
        self::assertTrue( $entity->hasProperty( 'second' ) );
    }
}