<?php

declare( strict_types = 1 );
namespace DTNL\SfoClient\Tests\Entity;

use \PHPUnit\Framework\TestCase;
use DTNL\SfoClient\Tests\TestClass\MockEntityFactory;
use DTNL\SfoClient\Tests\TestClass\ConcreteSfoEntity;
use DTNL\SfoClient\Metadata\MetadataRepository;
use DTNL\SfoClient\Metadata\Interfaces\SfoMetadataRepositoryInterface;
use DTNL\SfoClient\Metadata\EntityPropertyMetadata;
use DTNL\SfoClient\Metadata\EntityMetadata;
use DTNL\SfoClient\Entity\SfoEntityFactory;
use DTNL\SfoClient\Entity\Mapper\XmlSfoEntityMapper;

/**
 * @covers DTNL\SfoClient\Entity\Mapper\XmlSfoEntityMapper
 */
class XmlSfoEntityMapperTest extends TestCase {

    /** @var \SimpleXMLElement */
    protected $xml;

    /** @var array */
    protected $expected_properties;

    public function setUp() : void {

        $data = file_get_contents( __DIR__ . '/../TestData/jobrequisition.xml' );
        if ( $data === false ) {
            throw new \RuntimeException(
                'Not able to lead Job Requision XML data.'
            );
        }

        $this->xml = new \SimpleXMLElement( $data );

        $this->expected_properties = [
            'jobReqId' => 123,
            'corporatePosting' => false,
            'createdDateTime' => \DateTime::createFromFormat(
                    \DateTimeInterface::ATOM,
                    '2019-09-01T09:00:00Z'
                ),
            'salaryMax' => 40000.12,
            'isDraft' => null,
            'defaultLanguage' => 'en_GB',
        ];
    }

    public function testMapping() : void {
        $entity = ( new XmlSfoEntityMapper( $this->xml ) )
            ->setEntityFactory( new MockEntityFactory() )
            ->map( new ConcreteSfoEntity() );

        foreach ( $this->expected_properties as $k => $v ) {
            self::assertEquals( $v, $entity[ $k ] );
        }
    }

}
