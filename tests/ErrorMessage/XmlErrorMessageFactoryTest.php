<?php

declare( strict_types = 1 );
namespace DTNL\SfoClient\Tests\ErrorMessage;

use \PHPUnit\Framework\TestCase;
use DTNL\SfoClient\Tests\TestClass\ConcreteSfoEntity;
use DTNL\SfoClient\ErrorMessage\XmlSfoErrorMessageFactory;
use DTNL\SfoClient\Entity\Mapper\XmlSfoEntityMapper;

/**
 * @covers DTNL\SfoClient\Entity\Mapper\XmlSfoEntityMapper
 */
class XmlErrorMessageFactoryTest extends TestCase {

    /** @var \SimpleXMLElement */
    protected $xml;
    
    public function setUp() : void {
        $data = file_get_contents( __DIR__ . '/../TestData/error.xml' );
        if ( $data === false ) {
            throw new \RuntimeException(
                'Not able to lead Error XML data.'
            );
        }
        $this->xml = new \SimpleXMLElement( $data );
    }

    public function testCreation() : void {

        $error = XmlSfoErrorMessageFactory::create( $this->xml );
        
        self::assertEquals(
            'NotFoundException',
            $error->getCode()
        );

        self::assertEquals(
            'Entity TestEntity is not found.',
            $error->getMessage()
        );
    }

}
