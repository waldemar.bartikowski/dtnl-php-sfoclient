<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\ResponseParser;

use Psr\Http\Message\ResponseInterface;
use DTNL\SfoClient\ResponseParser\Interfaces\SfoResponseParserInterface;
use DTNL\SfoClient\ResponseParser\Exceptions\UnsupportedResponseFormatException;
use DTNL\SfoClient\Metadata\Interfaces\SfoMetadataRepositoryInterface;
use DTNL\SfoClient\Entity\SfoEntityFactory;

class SfoResponseParser implements SfoResponseParserInterface {

    /**
     * {@inheritDoc}
     * 
     * @throws UnsupportedResponseFormatException
     */
    public static function parse( ResponseInterface $response ) {

        $content_type = $response->getHeader( 'Content-Type' );
        $parts = explode( ';', $content_type[0], 2 );
        $media_type = strtolower( $parts[0] );
        
        switch ( $media_type ) {
            case 'application/atom+xml':
            case 'application/xml':
                return XmlSfoResponseParser::parse( $response );
            case 'application/json':
                throw new UnsupportedResponseFormatException();
            default:
                throw new UnsupportedResponseFormatException();
        }
    }
}