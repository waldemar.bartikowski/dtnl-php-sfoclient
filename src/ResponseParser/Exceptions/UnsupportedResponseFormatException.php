<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\ResponseParser\Exceptions;

class UnsupportedResponseFormatException extends SfoResponseException {};