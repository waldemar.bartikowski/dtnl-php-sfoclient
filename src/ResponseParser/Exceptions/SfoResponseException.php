<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\ResponseParser\Exceptions;

use DTNL\SfoClient\Exceptions\SfoClientException;

class SfoResponseException extends SfoClientException {};