<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\ResponseParser;

use Psr\Http\Message\ResponseInterface;
use DTNL\SfoClient\ResponseParser\Interfaces\SfoResponseParserInterface;
use DTNL\SfoClient\ResponseParser\Exceptions\UnsupportedResponseTypeException;
use DTNL\SfoClient\Metadata\XmlMetadataResponseParser;
use DTNL\SfoClient\Metadata\Interfaces\SfoMetadataRepositoryInterface;
use DTNL\SfoClient\ErrorMessage\XmlSfoErrorMessageFactory;
use DTNL\SfoClient\Entity\SfoEntityFactory;
use DTNL\SfoClient\Entity\Mapper\XmlSfoEntityMapperFactory;

class XmlSfoResponseParser implements SfoResponseParserInterface {

    /**
     * {@inheritDoc}
     * 
     * @throws UnsupportedResponseTypeException
     */
    public static function parse( ResponseInterface $response ) {
    
        $body = (string) $response->getBody();
        $xml = new \SimpleXMLElement( $body );
        $root_element_name = $xml->getName();

        $entity_factory = new SfoEntityFactory(
            new XmlSfoEntityMapperFactory()
        );
        
        switch ( $root_element_name ) {

            case 'entry':
                return $entity_factory
                    ->createEntity( $xml );

            case 'feed':
                $entities = [];
                foreach ( $xml->entry as $entry ) {
                    $entities[] = $entity_factory
                        ->createEntity( $entry );
                }
                return $entities;

            case 'error':
                return XmlSfoErrorMessageFactory::create( $xml );

            default:
                throw new UnsupportedResponseTypeException(
                    'Unsupported response type "'
                    . $root_element_name
                    . '"'
                );
        }
    }
}