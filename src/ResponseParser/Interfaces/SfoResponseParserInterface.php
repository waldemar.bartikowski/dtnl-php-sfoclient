<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\ResponseParser\Interfaces;

use Psr\Http\Message\ResponseInterface;
use DTNL\SfoClient\Metadata\Interfaces\SfoMetadataRepositoryInterface;
use DTNL\SfoClient\ErrorMessage\Interfaces\SfoErrorMessageInterface;
use DTNL\SfoClient\Entity\Interfaces\SfoEntityInterface;

interface SfoResponseParserInterface {

    /**
     * @param ResponseInterface $response
     * @return SfoEntityInterface|array|SfoErrorMessageInterface|SfoMetadataRepositoryInterface
     */
    public static function parse( ResponseInterface $response );

}