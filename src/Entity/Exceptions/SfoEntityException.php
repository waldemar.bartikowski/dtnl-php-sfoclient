<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Entity\Exceptions;

use DTNL\SfoClient\Exceptions\SfoClientException;

class SfoEntityException extends SfoClientException {};