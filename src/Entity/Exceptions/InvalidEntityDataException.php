<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Entity\Exceptions;

use DTNL\SfoClient\Entity\Exceptions\SfoEntityException;

class InvalidEntityDataException extends SfoEntityException {};