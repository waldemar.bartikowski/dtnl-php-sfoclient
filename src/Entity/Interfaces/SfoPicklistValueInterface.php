<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Entity\Interfaces;

interface SfoPicklistValueInterface {
    /**
     * @return string|int
     */
    public function getKey();
    public function getOptionId() : int;
    public function getLabel() : string;
    public function getLabelTranslations() : array;
    public function isActive() : bool;
}