<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Entity\Interfaces;

interface SfoEntityInterface extends \ArrayAccess, \IteratorAggregate, \JsonSerializable {

    /**
     * Get name of the entity type.
     *
     * @return string
     */
    public function getName() : string;

    /**
     * @param string $name
     * @param mixed $value
     * @return SfoEntityInterface
     */
    public function setProperty( string $name, $value ) : SfoEntityInterface;

    /**
     * @param string $name
     * @return mixed
     */
    public function getProperty( string $name );

    /**
     * @param string $name
     * @return boolean
     */
    public function hasProperty( string $name ) : bool;

    /**
     * @param string $name
     * @return SfoEntityInterface
     */
    public function removeProperty( string $name ) : SfoEntityInterface;
}