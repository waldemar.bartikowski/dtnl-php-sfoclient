<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Entity\Interfaces;

interface SfoEntityFactoryInterface {
    
    /**
     * @param mixed $entry
     * @return SfoEntityInterface
     */
    public function createEntity( $entry ) : SfoEntityInterface;
}