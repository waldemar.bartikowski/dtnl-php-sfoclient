<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Entity\Interfaces;

interface SfoPicklistInterface {
    /**
     * @return string|int
     */
    public function getId();
    public function getValues() : array;
}