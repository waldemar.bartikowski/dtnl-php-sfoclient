<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Entity\Interfaces;

use DTNL\SfoClient\Entity\Interfaces\SfoEntityMapperInterface;

interface SfoEntityMapperFactoryInterface {
    /**
     * @param mixed $xml
     * @return SfoEntityMapperInterface
     */
    public function create( $xml ) : SfoEntityMapperInterface;
}