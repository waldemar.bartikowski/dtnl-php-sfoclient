<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Entity\Interfaces;

use DTNL\SfoClient\Entity\Interfaces\SfoEntityMapperInterface;

interface SfoEntityMapperInterface {

    /**
     * @return string
     */
    public function getType() : string;

    /**
     * @param SfoEntityInterface $entity
     * @return SfoEntityInterface
     */
    public function map( SfoEntityInterface $entity ) : SfoEntityInterface;

    /**
     * Provide a Mapper Factory for recursive entity mapping.
     *
     * @param SfoEntityFactoryInterface $entity_factory
     * @return SfoEntityMapperInterface
     */
    public function setEntityFactory( SfoEntityFactoryInterface $entity_factory ) : SfoEntityMapperInterface;
}