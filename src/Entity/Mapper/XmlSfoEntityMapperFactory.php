<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Entity\Mapper;

use DTNL\SfoClient\Entity\Mapper\XmlSfoEntityMapper;
use DTNL\SfoClient\Entity\Interfaces\SfoEntityMapperInterface;
use DTNL\SfoClient\Entity\Interfaces\SfoEntityMapperFactoryInterface;

class XmlSfoEntityMapperFactory implements SfoEntityMapperFactoryInterface {

    /**
     * @param mixed $xml
     * @return SfoEntityMapperInterface
     */
    public function create( $xml ) : SfoEntityMapperInterface {
        return new XmlSfoEntityMapper( $xml );
    }
}