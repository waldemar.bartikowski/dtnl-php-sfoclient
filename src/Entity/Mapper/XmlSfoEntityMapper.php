<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Entity\Mapper;

use DTNL\SfoClient\Entity\Interfaces\SfoEntityMapperInterface;
use DTNL\SfoClient\Entity\Interfaces\SfoEntityInterface;
use DTNL\SfoClient\Entity\Interfaces\SfoEntityFactoryInterface;
use DTNL\SfoClient\Entity\Exceptions\InvalidEntityDataException;
use DTNL\SfoClient\Entity\Exceptions\EntityMapperException;
use DTNL\OdataClient\Metadata\PrimitiveMapper;

class XmlSfoEntityMapper implements SfoEntityMapperInterface {

    /** @var \SimpleXMLElement */
    protected $entry;

    /** @var SfoEntityFactoryInterface|null */
    protected $entity_factory;

    public function __construct( \SimpleXMLElement $entry ) {
        $this->entry = $entry;
        $this->throwExceptionIfNotEntryData();
    }

    public function setEntityFactory( SfoEntityFactoryInterface $entity_factory ) : SfoEntityMapperInterface {
        $this->entity_factory = $entity_factory;
        return $this;
    }

    public function map( SfoEntityInterface $entity ) : SfoEntityInterface {
        $entity = $this->mapProperties( $entity );
        $entity = $this->mapLinkedEntries( $entity );
        return $entity;
    }

    public function getType() : string {
        
        $category_attr = $this->entry->category->attributes();
        if ( $this->entry->category->count() === 0
          || is_null( $category_attr ) ) {
            throw new InvalidEntityDataException(
                'Type information not found.'
            );
        }

        return (string) $category_attr->term;
    }
    
    protected function mapProperties( SfoEntityInterface $entity ) : SfoEntityInterface {

        $properties = $this->entry
            ->content->children( 'm', true )
            ->properties->children  ( 'd', true );

        foreach ( $properties as $k => $v ) {
            $type = $v->attributes( 'm', true )->type;
            if ( isset( $type ) ) {
                $entity[$k] = PrimitiveMapper::map(
                    (string) $type,
                    (string) $v
                );
            } else {
                $entity[$k] = (string) $v;
            }
        }

        return $entity;
    }

    protected function mapLinkedEntries( SfoEntityInterface $entity ) : SfoEntityInterface {

        if ( !isset( $this->entity_factory ) ) {
            throw new EntityMapperException(
                self::class . ' needs an Entity Factory to be able to map linked elements.'
            );
        }
        
        $links = $this->entry->link;
        
        foreach ( $links as $link ) {

            $linked_property_name = (string) $link->attributes()->title;
            $linked_propery_data = null;

            if ( $link->children( 'm', true )->count() == 0 ) {
                continue;
            }

            $inline_element = $link ->children( 'm', true ) ->inline;
            if ( $inline_element->count() == 0 ) {
                continue;
            }

            $inline_entries = $inline_element->children();
            $root_element_name = $inline_entries->getName();

            switch ( $root_element_name ) {

                case 'feed':
                    $entries = [];
                    foreach ( $inline_entries->feed->entry as $entry ) {
                        $entries[] = $this->entity_factory
                            ->createEntity( $entry );
                    }
                    $linked_property_data = $entries;
                    break;

                case 'entry':
                    $linked_property_data = $this->entity_factory
                        ->createEntity( $inline_entries->entry );
                    break;

                default;
                    $linked_property_data = null;
            }

            $entity[ $linked_property_name ] = $linked_property_data;
        }

        return $entity;
    }

    private function throwExceptionIfNotEntryData() : void {
        
        if ( $this->entry->getName() !== 'entry' ) {
            throw new InvalidEntityDataException(
                'Element needs to be "entry".'
            );
        }

    }
}