<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Entity\Type;

use DTNL\SfoClient\Entity\Interfaces\SfoPicklistInterface;
use DTNL\SfoClient\Entity\AbstractSfoEntity;

class PicklistV2 extends AbstractSfoEntity implements SfoPicklistInterface {

    public function getId() {
        return $this->getProperty( 'id' );
    }
    
    public function getValues() : array {
        return $this->getProperty( 'values' );
    }
}