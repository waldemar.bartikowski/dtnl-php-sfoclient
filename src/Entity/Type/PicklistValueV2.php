<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Entity\Type;

use DTNL\SfoClient\Entity\Interfaces\SfoPicklistValueInterface;
use DTNL\SfoClient\Entity\AbstractSfoEntity;

class PicklistValueV2 extends AbstractSfoEntity  implements SfoPicklistValueInterface {
    
    public function getKey() {
        return $this->getProperty( 'externalCode' );
    }

    public function getLabel() : string {
        return $this->getProperty( 'label_defaultValue' );
    }

    public function getOptionId() : int {
        return $this->getProperty( 'optionId' );
    }

    public function getLabelTranslations() : array {
        $translations = $this->getProperty( 'labelTranslationTextNav' );
        $labels = [];
        foreach ( $translations as $translation ) {
            $locale = $translation->getProperty( 'locale' );
            $value = $translation->getProperty( 'value' );
            if ( $value == '' ) { continue; }
            $labels[ $locale ]  = $value;
        }
        return $labels;
    }

    public function isActive() : bool {
        return $this->getProperty( 'status' ) === 'A';
    }
}