<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Entity\Type;

use DTNL\SfoClient\Entity\Interfaces\SfoPicklistValueInterface;
use DTNL\SfoClient\Entity\AbstractSfoEntity;

class PicklistOption extends AbstractSfoEntity implements SfoPicklistValueInterface {
    
    public function getKey() {
        return $this->getProperty( 'id' );
    }

    public function getOptionId() : int {
        return (int) $this->getKey();
    }

    public function getLabel() : string {

        $label_translations = $this->getLabelTranslations();

        if ( count( $label_translations ) == 0 ) { return ''; }

        if ( isset( $label_translations['en_GB'] ) ) {
            return $label_translations['en_GB'];
        } else if ( isset( $label_translations['en_US'] ) ) {
            return $label_translations['en_US'];
        }

        return $label_translations[0];
    }

    public function getLabelTranslations() : array {
        $translations = $this->getProperty( 'picklistLabels' );
        $labels = [];
        foreach ( $translations as $translation ) {
            $locale = $translation->getProperty( 'locale' );
            $value = $translation->getProperty( 'label' );
            if ( $value == '' ) { continue; }
            $labels[ $locale ]  = $value;
        }
        return $labels;
    }

    public function isActive() : bool {
        // Not implemented here, just return TRUE.
        return TRUE;
    }
}