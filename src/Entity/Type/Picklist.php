<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Entity\Type;

use DTNL\SfoClient\Entity\Interfaces\SfoPicklistInterface;
use DTNL\SfoClient\Entity\AbstractSfoEntity;

class Picklist extends AbstractSfoEntity implements SfoPicklistInterface {
    
    public function getId() {
        return $this->getProperty( 'picklistId' );
    }
    
    public function getValues() : array {
        return $this->getProperty( 'picklistOptions' );
    }
}