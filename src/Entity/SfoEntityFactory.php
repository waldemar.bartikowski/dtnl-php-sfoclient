<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Entity;

use DTNL\SfoClient\Entity\Type\PicklistValueV2;
use DTNL\SfoClient\Entity\Type\PicklistV2;
use DTNL\SfoClient\Entity\Type\PicklistOption;
use DTNL\SfoClient\Entity\Type\Picklist;
use DTNL\SfoClient\Entity\Type\JobRequisition;
use DTNL\SfoClient\Entity\Mapper\XmlSfoEntityMapper;
use DTNL\SfoClient\Entity\Interfaces\SfoEntityMapperInterface;
use DTNL\SfoClient\Entity\Interfaces\SfoEntityMapperFactoryInterface;
use DTNL\SfoClient\Entity\Interfaces\SfoEntityInterface;
use DTNL\SfoClient\Entity\Interfaces\SfoEntityFactoryInterface;
use DTNL\SfoClient\Entity\GenericSfoEntity;
use DTNL\SfoClient\Entity\Exceptions\InvalidEntityDataException;

class SfoEntityFactory implements SfoEntityFactoryInterface {

    /** @var SfoEntityMapperFactoryInterface */
    protected $mapper_factory;

    public function __construct( SfoEntityMapperFactoryInterface $mapper_factory ) {
        $this->mapper_factory = $mapper_factory;
    }

    /**
     * @param mixed $data
     * @return SfoEntityInterface
     */
    public function createEntity( $data ) : SfoEntityInterface {

        /** @var SfoEntityMapperInterface $mapper */
        $mapper = $this->mapper_factory->create( $data );
        $mapper->setEntityFactory( $this );

        $type = $mapper->getType();

        switch ( $type ) {
            case 'SFOData.JobRequisition':
                $entity = new JobRequisition( $type );
                break;
            case 'SFOData.Picklist':
                $entity = new Picklist( $type );
                break;
            case 'SFOData.PickListV2':
                $entity = new PicklistV2( $type );
                break;
            case 'SFOData.PicklistOption':
                $entity = new PicklistOption( $type );
                break;
            case 'SFOData.PickListValueV2':
                $entity = new PicklistValueV2( $type );
                break;
            default:
                $entity = new GenericSfoEntity( $type );
        }

        $entity = $mapper->map( $entity );
        
        return $entity;
    }
}