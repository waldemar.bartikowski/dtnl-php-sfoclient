<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Entity;

use DTNL\SfoClient\Entity\Interfaces\SfoEntityInterface;

abstract class AbstractSfoEntity implements SfoEntityInterface {

    const UNDEFINED_NAME = 'undefined';

    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $properties;

    /**
     * @param string $name
     */
    public function __construct( string $name = self::UNDEFINED_NAME ) {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName() : string {
        return $this->name;
    }

    /**
     * @param string $name
     * @param mixed $value
     * @return SfoEntityInterface
     */
    public function setProperty( string $name, $value ) : SfoEntityInterface {
        $this->properties[ $name ] = $value;
        return $this;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function getProperty( string $name ) {
        return $this->properties[ $name ];
    }

    /**
     * @param string $name
     * @return boolean
     */
    public function hasProperty( string $name ) : bool {
        return isset( $this->properties[ $name ] );
    }

    /**
     * @param string $name
     * @return SfoEntityInterface
     */
    public function removeProperty( string $name ) : SfoEntityInterface {
        unset( $this->properties[ $name ] );
        return $this;
    }

    /**
     * Implement ArrayAccess
     *
     * @param mixed $offset
     * @return boolean
     */
    public function offsetExists( $offset ) : bool {
        return $this->hasProperty( $offset );
    }

    /**
     * Implement ArrayAccess
     *
     * @param mixed $offset
     * @return mixed
     */
    public function offsetGet( $offset ) {
        return $this->getProperty( $offset );
    }

    /**
     * Implement ArrayAccess
     *
     * @param mixed $offset
     * @param mixed $value
     * @return void
     */
    public function offsetSet( $offset, $value ) : void {
        $this->setProperty( $offset, $value );
    }

    /**
     * Implement ArrayAccess
     *
     * @param mixed $offset
     * @return void
     */
    public function offsetUnset( $offset ) : void {
        $this->removeProperty( $offset );
    }

    /**
     * Implement IteratorAggregate
     *
     * @return \Traversable
     */
    public function getIterator() : \Traversable {
        yield from $this->properties;
    }

    /**
     * Return string representation
     *
     * @return string
     */
    public function __toString() : string {
        $string = static::class . ' {' . PHP_EOL;
        foreach ( $this->properties as $k => $v ) {
            
            if ( $v instanceof \DateTime ) {
                $v = $v->format( \DateTimeInterface::ATOM );
            }

            if ( is_bool( $v ) ) { $v = $v ? 'true' : 'false'; }

            $string .= '  ' . $k . ': ' . $v . PHP_EOL;
        }
        $string .= '}' . PHP_EOL;
        return $string;
    }

    /**
     * Implement \JsonSerializable
     * 
     * @return array
     */
    public function jsonSerialize() : array {

        $json_data = [];

        if ( $this->getName() != self::UNDEFINED_NAME ) {
            $json_data = [
                '__metadata' => [
                    'type' => $this->getName(),
                ]
            ];
        }

        foreach ( $this->properties as $k => $v ) {
            
            if ( $v instanceof \DateTime ) {
                $json_data[ $k ] = '/Date(' . $v->format( 'Uv' ) . ')/';
                continue;
            }

            $json_data[ $k ] = $v;
        }

        return $json_data;
    }
}