<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Client;

use Psr\Http\Message\UriInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\RequestInterface;
use DTNL\SfoClient\ResponseParser\SfoResponseParser;
use DTNL\SfoClient\ResponseParser\Exceptions\UnsupportedResponseFormatException;
use DTNL\SfoClient\ErrorMessage\SfoErrorMessage;
use DTNL\SfoClient\Entity\Interfaces\SfoEntityInterface;
use DTNL\SfoClient\Client\Interfaces\SfoClientInterface;
use DTNL\SfoClient\Client\Exceptions\SfoClientUpsertException;
use DTNL\SfoClient\Client\Exceptions\SfoClientRequestException;
use DTNL\SfoClient\Client\Exceptions\SfoClientClientException;
use DTNL\OdataClient\Request\StreamFactory;
use DTNL\OdataClient\Request\OdataRequestBuilder;
use DTNL\OdataClient\Request\Interfaces\OdataRequestInterface;
use DTNL\OdataClient\Request\Interfaces\OdataRequestBuilderInterface;
use DTNL\OdataClient\Expression\ExpressionFactory as Expr;
use DTNL\OdataClient\Client\Interfaces\OdataRequestExceptionInterface;
use DTNL\OdataClient\Client\Interfaces\OdataClientInterface;

class SfoClient implements SfoClientInterface {

    /** @var OdataClientInterface */
    protected $odata_client;

    /** @var UriInterface|string */
    protected $service_root = '';

    public function __construct( OdataClientInterface $odata_client ) {
        $this->odata_client = $odata_client;
    }

    /**
     * Send a Odata Request to SF.
     *
     * @param OdataRequestInterface $request
     * @return ResponseInterface
     * 
     * @throws SfoClientRequestException
     * @throws UnsupportedResponseFormatException
     */
    private function sendRequest( OdataRequestInterface $request ) : ResponseInterface {
        try {
            $response = $this->odata_client->sendRequest( $request );
        } catch ( OdataRequestExceptionInterface $e ) {
            try {
                /** @var \DTNL\SfoClient\ErrorMessage\Interfaces\SfoErrorMessageInterface */
                $error_message = SfoResponseParser::parse( $e->getResponse() );
                throw new SfoClientRequestException(
                    $error_message,
                    $e->getRequest(),
                    $e->getResponse()
                );
            } catch ( UnsupportedResponseFormatException $uncaught_exception ) {
                throw $e;
            }
        }
        return $response;
    }

    /**
     * @param \DTNL\OdataClient\Request\Interfaces\OdataResourcePathInterface|string $resource_path
     * @param \DTNL\OdataClient\Parameter\Interfaces\ParameterInterface[] $parameters
     * @return OdataRequestInterface
     */
    private function createRequest(
        $resource_path,
        array $parameters = []
    ) : OdataRequestInterface {
        $request_builder = $this->setUpAndReturnRequestBuilder();
        $request_builder->setResourcePath( $resource_path );
        
        $request = $request_builder->build();
        foreach ( $parameters as $parameter ) {
            $request->addParameter( $parameter );
        }

        return $request;
    }

    /**
     * @param string $name
     * @param integer $key
     * @return ResponseInterface
     */
    public function getEntity( string $name, int $key ) : ResponseInterface {
        $request = $this->createRequest( $name . '(' . $key . ')' );
        return $this->sendRequest( $request );
    }

    /**
     * Create a new Entity entry.
     *
     * @param SfoEntityInterface $entity
     * @return ResponseInterface
     * 
     * @throws SfoClientClientException
     */
    public function createEntity( SfoEntityInterface $entity ) : ResponseInterface {

        $entity_wo_namespace = $this->getEntityNameWithoutNamespace( $entity );
        
        $request_builder = new OdataRequestBuilder(
            OdataRequestInterface::POST,
            (string) $this->service_root,
            $entity_wo_namespace
        );

        $request = $request_builder->build();
        $body_stream = $this->createStreamFromEntity( $entity );
        $request->setBody( $body_stream, 'application/json' );

        return $this->sendRequest( $request );
    }

    /**
     * @param SfoEntityInterface $entity
     * @param integer $key
     * @return ResponseInterface
     */
    public function updateEntity( SfoEntityInterface $entity, int $key ) : ResponseInterface {
        
        $entity_wo_namespace = $this->getEntityNameWithoutNamespace( $entity );
        
        $request_builder = new OdataRequestBuilder(
            OdataRequestInterface::PUT,
            (string) $this->service_root,
            $entity_wo_namespace . '(' . $key . ')'
        );

        $request = $request_builder->build();
        $body_stream = $this->createStreamFromEntity( $entity );
        $request->setBody( $body_stream, 'application/json' );

        return $this->sendRequest( $request );
    }

    /**
     * @param SfoEntityInterface $entity
     * @return ResponseInterface
     */
    public function upsertEntity( SfoEntityInterface $entity ) : ResponseInterface {
        
        $request_builder = new OdataRequestBuilder(
            OdataRequestInterface::POST,
            (string) $this->service_root,
            'upsert'
        );

        $request_builder->setFormat( 'json' );

        $request = $request_builder->build();
        $body_stream = $this->createStreamFromEntity( $entity );
        $request->setBody( $body_stream, 'application/json' );

        $response = $this->sendRequest( $request );

        $this->checkUpsertResponseAndThrowExceptions( $response );

        return $response;
    }

    /**
     * @param ResponseInterface $response
     * @return void
     * @throws SfoClientUpsertException
     */
    protected function checkUpsertResponseAndThrowExceptions( ResponseInterface $response ) : void {
        
        $data = json_decode( (string) $response->getBody() );
        
        if ( !isset( $data->d[0]->status ) ) {
            throw new SfoClientUpsertException(
                new SfoErrorMessage( '', 'Invalid upsert response.' ),
                $response
            );
        }

        if ( $data->d[0]->status !== 'OK' ) {
            if ( isset( $data->d[0]->httpCode ) && isset( $data->d[0]->message ) ) {
                throw new SfoClientUpsertException(
                    new SfoErrorMessage(
                        $data->d[0]->httpCode . ' ' .$data->d[0]->status,
                        $data->d[0]->message
                    ),
                    $response
                );
            } else {
                throw new SfoClientUpsertException(
                    new SfoErrorMessage( '', 'Upsert failed.' ),
                    $response
                );
            }
        }

    }

    /**
     * @param SfoEntityInterface $entity
     * @return string
     */
    private function getEntityNameWithoutNamespace( SfoEntityInterface $entity ) : string {

        if ( preg_match( '/[^.]+$/', $entity->getName(), $result ) === FALSE ) {
            throw new SfoClientClientException(
                'Missing / invalid Entity name: "' . $entity->getName() . '".'
            );
        }
        return $result[0];
    }

    /**
     * @param SfoEntityInterface $entity
     * @return StreamInterface
     */
    private function createStreamFromEntity( SfoEntityInterface $entity ) : StreamInterface {
        
        $json_data = json_encode(
            $entity,
            JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES
        );

        if ( $json_data === false ) {
            throw new SfoClientClientException( 'Invalid JSON data.' );
        }

        return StreamFactory::createFromString( $json_data );
    }

    /**
     * @param string $name
     * @param integer $key
     * @return ResponseInterface
     */
    public function deleteEntity( string $name, int $key ) : ResponseInterface {

        $request_builder = new OdataRequestBuilder(
            OdataRequestInterface::DELETE,
            (string) $this->service_root,
            $name  . '(' . $key . ')'
        );

        $request = $request_builder->build();

        return $this->sendRequest( $request );
    }

    /**
     * @param string $name
     * @return ResponseInterface
     */
    public function getEntityMetadata( ?string $name = null ) : ResponseInterface {
        if ( is_null( $name ) ) {
            $request = $this->createRequest( '$metadata' );
        } else {
            $request = $this->createRequest( $name . '/$metadata' );
        }
        return $this->sendRequest( $request );
    }

    /**
     * @param string $name
     * @param \DTNL\OdataClient\Parameter\Interfaces\ParameterInterface[] $parameters
     * @return ResponseInterface
     */
    public function getEntities( string $name, array $parameters = [] ) : ResponseInterface {
        $request = $this->createRequest( $name, $parameters );
        return $this->sendRequest( $request );
    }

    /**
     * @param ?string $name
     * @return ResponseInterface
     */
    public function getPicklist( ?string $name = null ) : ResponseInterface {
       
        $request_builder = $this->setUpAndReturnRequestBuilder();
        $request_builder
            ->setResourcePath(
                (string) Expr::entity( 'Picklist', $name )
            )
            ->setExpand(
                [
                    'picklistOptions',
                    'picklistOptions/picklistLabels',
                ]
            )
        ;

        $request = $request_builder->build();
        return $this->sendRequest( $request );
    }

    /**
     * @param string $name
     * @return ResponseInterface
     */
    public function getPicklistV2( ?string $name = null ) : ResponseInterface {
       
        $request_builder = $this->setUpAndReturnRequestBuilder();

        if ( isset( $name ) ) {
            $request_builder
                ->setResourcePath(
                    (string) Expr::entity(
                        'PickListV2',
                        Expr::list(
                            [
                                'id=' . (string) Expr::string( $name ),
                                'effectiveStartDate=datetime\'1900-01-01T00:00:00\''
                            ]
                        )
                    )
                );
        } else {
            $request_builder
                ->setResourcePath(
                    (string) Expr::entity( 'PickListV2' )
                );
        }

        $request_builder
            ->setExpand(
                [ 'values', 'values/labelTranslationTextNav', ]
            )
            ->setSelect(
                [ 'id', 'displayOrder', 'name', 'status', 'values' ]
            )
        ;  

        $request = $request_builder->build();

        return $this->sendRequest( $request );
    }

    /**
     * @param UriInterface|string $service_root
     * @return SfoClientInterface
     */
    public function setServiceRoot( $service_root ) : SfoClientInterface {
        $this->service_root = $service_root;
        return $this;
    }

    protected function setUpAndReturnRequestBuilder() : OdataRequestBuilderInterface {
        return ( new OdataRequestBuilder() )
            ->setServiceRoot( $this->service_root )
        ;
    }
}