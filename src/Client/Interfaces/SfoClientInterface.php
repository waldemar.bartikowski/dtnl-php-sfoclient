<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Client\Interfaces;

use Psr\Http\Message\UriInterface;
use Psr\Http\Message\ResponseInterface;

interface SfoClientInterface {
    /**
     * @param string $name
     * @param integer $key
     * @return ResponseInterface
     */
    public function getEntity( string $name, int $key ) : ResponseInterface;

    /**
     * @param string $name
     * @return ResponseInterface
     */
    public function getEntityMetadata( ?string $name = null ) : ResponseInterface;

    /**
     * @param string $name
     * @param \DTNL\OdataClient\Parameter\Interfaces\ParameterInterface[] $parameters
     * @return ResponseInterface
     */
    public function getEntities( string $name, array $parameters = [] ) : ResponseInterface;

    /**
     * @param ?string $name
     * @return ResponseInterface
     */
    public function getPicklist( ?string $name = null ) : ResponseInterface;

    /**
     * @param string $name
     * @return ResponseInterface
     */
    public function getPicklistV2( string $name ) : ResponseInterface;

    /**
     * @param UriInterface|string $service_root
     * @return SfoClientInterface
     */
    public function setServiceRoot( $service_root ) : SfoClientInterface;

}