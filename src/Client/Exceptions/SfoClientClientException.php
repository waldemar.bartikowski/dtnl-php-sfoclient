<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Client\Exceptions;

use DTNL\SfoClient\Exceptions\SfoClientException;

class SfoClientClientException extends SfoClientException {};