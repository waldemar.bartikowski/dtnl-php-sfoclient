<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Client\Exceptions;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\RequestInterface;
use DTNL\SfoClient\ErrorMessage\Interfaces\SfoErrorMessageInterface;

class SfoClientRequestException extends SfoClientClientException {

    /** @var SfoErrorMessageInterface  */
    protected $error_message;

    /** @var RequestInterface|null */
    protected $request;

    /** @var ResponseInterface|null */
    protected $response;

    public function __construct(
        SfoErrorMessageInterface $error_message,
        ?RequestInterface $request,
        ?ResponseInterface $response
    ) {
        $this->error_message = $error_message;
        $this->request = $request;
        $this->response = $response;

        parent::__construct( $this->createMessage() );
    }

    protected function createMessage() : string {
        return $this->error_message->getCode()
            . ': ' . $this->error_message->getMessage();
    }

    public function getRequest() : ?RequestInterface {
        return $this->request;
    }

    public function getResponse() : ?ResponseInterface {
        return $this->response;
    }
}

