<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Client\Exceptions;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\RequestInterface;
use DTNL\SfoClient\ErrorMessage\Interfaces\SfoErrorMessageInterface;
use DTNL\SfoClient\Client\Exceptions\SfoClientRequestException;

class SfoClientUpsertException extends SfoClientRequestException {
    
    public function __construct(
        SfoErrorMessageInterface $error_message,
        ResponseInterface $response
    ) {
        parent::__construct( $error_message, NULL, $response );
    }

    public function getRequest() : ?RequestInterface {
        if ( !isset( $this->request ) ) {
            throw new \RuntimeException('Request not provided.');
        } else {
            return parent::getRequest();
        }
    }

}