<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Exceptions;

class SfoClientException extends \Exception {};