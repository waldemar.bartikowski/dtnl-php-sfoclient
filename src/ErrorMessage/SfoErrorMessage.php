<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\ErrorMessage;

use DTNL\SfoClient\ErrorMessage\Interfaces\SfoErrorMessageInterface;

class SfoErrorMessage implements SfoErrorMessageInterface {

    /** @var string */
    protected $code;

    /** @var string */
    protected $message;

    public function __construct( string $code, string $message ) {
        $this->code = $code;
        $this->message = $message;
    }

    public function getCode() : string {
        return $this->code;
    }

    public function getMessage() : string {
        return $this->message;
    }
}