<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\ErrorMessage\Interfaces;

interface SfoErrorMessageInterface {
    public function getCode() : string;
    public function getMessage() : string;
}