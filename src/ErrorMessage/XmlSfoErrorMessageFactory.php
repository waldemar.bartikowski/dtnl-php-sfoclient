<?php
namespace DTNL\SfoClient\ErrorMessage;

use DTNL\SfoClient\ErrorMessage\Interfaces\SfoErrorMessageInterface;

class XmlSfoErrorMessageFactory {
    public static function create( \SimpleXMLElement $xml ) : SfoErrorMessageInterface {
        return new SfoErrorMessage(
            (string) $xml->code,
            (string) $xml->message
        );
    }
}