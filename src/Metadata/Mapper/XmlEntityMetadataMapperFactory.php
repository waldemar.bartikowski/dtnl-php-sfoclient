<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Metadata\Mapper;

use DTNL\SfoClient\Metadata\Interfaces\SfoEntityMetadataMapperInterface;
use DTNL\SfoClient\Metadata\Interfaces\SfoEntityMetadataMapperFactoryInterface;

class XmlEntityMetadataMapperFactory implements SfoEntityMetadataMapperFactoryInterface {

    /**
     * @param mixed $xml
     * @return SfoEntityMetadataMapperInterface
     */
    public function create( $xml ) : SfoEntityMetadataMapperInterface {
        return new XmlEntityMetadataMapper( $xml );
    }
}