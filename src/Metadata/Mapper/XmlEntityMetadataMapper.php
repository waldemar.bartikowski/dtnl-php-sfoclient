<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Metadata\Mapper;

use DTNL\SfoClient\Metadata\Interfaces\SfoEntityMetadataMapperInterface;
use DTNL\SfoClient\Metadata\Interfaces\SfoEntityMetadataInterface;
use DTNL\SfoClient\Metadata\Exceptions\InvalidMetadataException;
use DTNL\SfoClient\Metadata\EntityPropertyMetadata;
use DTNL\SfoClient\Metadata\EntityNavigationPropertyMetadata;
use DTNL\SfoClient\Metadata\EntityMetadata;

class XmlEntityMetadataMapper implements SfoEntityMetadataMapperInterface {

    /** @var \SimpleXMLElement */
    protected $entity;

    public function __construct( \SimpleXMLElement $xml ) {
        $this->entity = $xml;
        $this->throwExceptionIfNotEntryData();
    }

    public function map( ?string $namespace = null ) : SfoEntityMetadataInterface {

        $attributes = $this->entity->attributes();
        if ( is_null( $attributes ) ) {
            throw new InvalidMetadataException(
                'Entity metadata is missing attributes.'
            );
        }

        $name = (string) $attributes->Name;
        $entity_metadata = new EntityMetadata( $name, $namespace );

        if ( !is_null( $this->entity->Key->PropertyRef->attributes() ) ) {
            $key = (string) $this->entity->Key->PropertyRef->attributes()->Name;
            $entity_metadata->setKey( $key );
        }

        $entity_metadata = $this->mapPropertyMetadata( $entity_metadata );
        $entity_metadata = $this->mapNavigationPropertyMetadata( $entity_metadata );

        return $entity_metadata;
    }

    private function mapPropertyMetadata(
        SfoEntityMetadataInterface $entity_metadata
    ) : SfoEntityMetadataInterface {

        $properties = $this->entity->Property;

        if ( $properties->count() === 0 ) {
            return $entity_metadata;
        }

        foreach ( $properties as $property ) {
            $attributes = $property->attributes();
            if ( is_null( $attributes ) ) {
                throw new InvalidMetadataException(
                    'Property metadata is missing attributes.'
                );
            }
            
            $property_name = (string) $attributes->Name;
            $property_type = (string) $attributes->Type;
            $property_nullable = ( (string) $attributes->Nullable ) == 'true';

            $property_metadata = new EntityPropertyMetadata(
                $property_name, $property_type, $property_nullable
            );

            $entity_metadata->addProperty( $property_metadata );
        }
        
        return $entity_metadata;
    }

    private function mapNavigationPropertyMetadata(
        SfoEntityMetadataInterface $entity_metadata
    ) : SfoEntityMetadataInterface {

        $properties = $this->entity->NavigationProperty;

        if ( $properties->count() === 0 ) {
            return $entity_metadata;
        }

        foreach ( $properties as $property ) {
            $attributes = $property->attributes();
            if ( is_null( $attributes ) ) {
                throw new InvalidMetadataException(
                    'NavigationProperty metadata is missing attributes.'
                );
            }
            
            $property_name = (string) $attributes->Name;
            $property_type = (string) $attributes->Type;
            $property_nullable = ( (string) $attributes->Nullable ) == 'true';

            $property_from_role = (string) $attributes->FromRole;
            $property_to_role = (string) $attributes->ToRole;
            $property_relationship = (string) $attributes->Relationship;

            $property_metadata = new EntityNavigationPropertyMetadata(
                $property_name, $property_type, $property_nullable,
                $property_from_role, $property_to_role, $property_relationship
            );

            $entity_metadata->addProperty( $property_metadata );
        }
        
        return $entity_metadata;
    }

    

    private function throwExceptionIfNotEntryData() : void {
        
        if ( $this->entity->getName() !== 'EntityType' ) {
            throw new InvalidMetadataException(
                'Element needs to be "EntityType".'
            );
        }

    }

}