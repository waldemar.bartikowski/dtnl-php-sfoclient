<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Metadata\Exceptions;

use DTNL\SfoClient\Metadata\Interfaces\SfoEntityMetadataInterface;
use DTNL\SfoClient\Exceptions\SfoClientException;

class SfoMetadataKeyNotDefinedException extends SfoMetadataException {

    /** @var SfoEntityMetadataInterface */
    private $entity_metadata;

    public function __construct(
        SfoEntityMetadataInterface $entity_metadata
    ) {
        $this->entity_metadata = $entity_metadata;
        parent::__construct();
    }

    public function getEntityMetadata() : SfoEntityMetadataInterface {
        return $this->entity_metadata;
    }

};