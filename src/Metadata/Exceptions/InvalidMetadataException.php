<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Metadata\Exceptions;

use DTNL\SfoClient\Metadata\Exceptions\SfoMetadataException;

class InvalidMetadataException extends SfoMetadataException {
};