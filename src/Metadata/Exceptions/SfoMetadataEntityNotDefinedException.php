<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Metadata\Exceptions;

use DTNL\SfoClient\Metadata\Interfaces\SfoEntityMetadataInterface;
use DTNL\SfoClient\Exceptions\SfoClientException;

class SfoMetadataEntityNotDefinedException extends SfoMetadataException {

    /** @var string */
    private $entity_name;

    public function __construct( string $entity_name ) {
        $this->entity_name = $entity_name;
        parent::__construct();
    }

    public function getEntityName() : string {
        return $this->entity_name;
    }

};