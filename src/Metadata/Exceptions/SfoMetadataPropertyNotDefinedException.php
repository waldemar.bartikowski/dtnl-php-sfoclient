<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Metadata\Exceptions;

use DTNL\SfoClient\Metadata\Interfaces\SfoEntityMetadataInterface;
use DTNL\SfoClient\Exceptions\SfoClientException;

class SfoMetadataPropertyNotDefinedException extends SfoMetadataException {


    /** @var SfoEntityMetadataInterface */
    private $entity_metadata;

    /** @var string */
    private $property_name;

    public function __construct(
        SfoEntityMetadataInterface $entity_metadata,
        string $property_name
    ) {
        $this->entity_metadata = $entity_metadata;
        $this->property_name = $property_name;
        parent::__construct(
            'Property "'
            . $property_name
            . '" not defined in metadata of "'
            . $entity_metadata->getName()
            . '" entity.'
        );
    }

    public function getEntityMetadata() : SfoEntityMetadataInterface {
        return $this->entity_metadata;
    }

    public function getPropertyName() : string {
        return $this->property_name;
    }

};