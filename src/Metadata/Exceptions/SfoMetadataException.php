<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Metadata\Exceptions;

use DTNL\SfoClient\Exceptions\SfoClientException;

class SfoMetadataException extends SfoClientException {};