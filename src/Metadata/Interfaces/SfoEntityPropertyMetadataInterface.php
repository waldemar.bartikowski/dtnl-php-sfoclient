<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Metadata\Interfaces;

interface SfoEntityPropertyMetadataInterface {

    /**
     * Get the name of the property.
     *
     * @return string
     */
    public function getName() : string;

    /**
     * Get the Type of the property.
     *
     * @return string
     */
    public function getType() : string;

    /**
     * Check if this property is nullable.
     *
     * @return boolean
     */
    public function isNullable() : bool;
}