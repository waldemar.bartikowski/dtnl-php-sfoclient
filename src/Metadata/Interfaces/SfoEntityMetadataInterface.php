<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Metadata\Interfaces;

interface SfoEntityMetadataInterface {

    /**
     * Get name *without* namespace.
     *
     * @return string
     */
    public function getName() : string;

    /**
     * Get the namespace of this Entity.
     *
     * @return string
     */
    public function getNamespace() : string;

    /**
     * Get the name *with* the namespace.
     *
     * @return string
     */
    public function getFullName() : string;

    /**
     * Set name of the key.
     * 
     * @var string $key
     * @return SfoEntityMetadataInterface
     */
    public function setKey( string $key ) : SfoEntityMetadataInterface;

    /**
     * Get name of the key.
     * 
     * @return string
     * @throws \DTNL\SfoClient\Metadata\Exceptions\SfoMetadataKeyNotDefinedException
     */
    public function getKey() : string;

    /**
     * Add property metadata.
     *
     * @param SfoEntityPropertyMetadataInterface $property
     * @return SfoEntityMetadataInterface
     */
    public function addProperty( SfoEntityPropertyMetadataInterface $property ) : SfoEntityMetadataInterface;

    /**
     * Get property metadata.
     *
     * @param string $name
     * @return SfoEntityPropertyMetadataInterface
     * @throws \DTNL\SfoClient\Metadata\Exceptions\SfoMetadataPropertyNotDefinedException
     */
    public function getProperty( string $name ) : SfoEntityPropertyMetadataInterface;

    /**
     * Check if property exists.
     * 
     * @return bool
     */
    public function hasProperty( string $name ) : bool;

    /**
     * Return metadata for all properties.
     * 
     * @return SfoEntityPropertyMetadataInterface[]
     */
    public function getProperties() : array;
}