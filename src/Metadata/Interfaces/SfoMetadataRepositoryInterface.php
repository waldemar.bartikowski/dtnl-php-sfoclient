<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Metadata\Interfaces;

interface SfoMetadataRepositoryInterface {

    /**
     * Set default namespace that is used to look for entities.
     *
     * @param ?string $namespace
     * @return SfoMetadataRepositoryInterface
     */
    public function setDefaultNamespace( ?string $namespace ) : SfoMetadataRepositoryInterface;

    /**
     * Add metadata for an entity.
     *
     * @param SfoEntityMetadataInterface $entity_metadata
     * @return SfoMetadataRepositoryInterface
     */
    public function addEntityMetadata( SfoEntityMetadataInterface $entity_metadata ) : SfoMetadataRepositoryInterface;

    /**
     * Check if metadata for an entity exists.
     *
     * @param string $name
     * @return boolean
     */
    public function hasEntityMetadata( string $name ) : bool;

    /**
     * Get the metadata for an entity.
     *
     * @param string $name
     * @return SfoEntityMetadataInterface
     * @throws \DTNL\SfoClient\Metadata\Exceptions\SfoMetadataEntityNotDefinedException
     */
    public function getEntityMetadata( string $name ) : SfoEntityMetadataInterface;


    /**
     * Get metadata for all entities.
     *
     * @return SfoEntityMetadataInterface[]
     */
    public function getAllEntityMetadata() : array;
}