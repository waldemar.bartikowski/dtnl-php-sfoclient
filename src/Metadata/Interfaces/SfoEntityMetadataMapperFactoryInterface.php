<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Metadata\Interfaces;

interface SfoEntityMetadataMapperFactoryInterface {
    /**
     * @param mixed $input
     * @return SfoEntityMetadataMapperInterface
     */
    public function create( $input ) : SfoEntityMetadataMapperInterface;
}