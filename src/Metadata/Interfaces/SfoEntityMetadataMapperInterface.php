<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Metadata\Interfaces;

interface SfoEntityMetadataMapperInterface {
    public function map() : SfoEntityMetadataInterface;
}