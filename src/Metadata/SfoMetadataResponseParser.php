<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Metadata;

use Psr\Http\Message\ResponseInterface;
use DTNL\SfoClient\ResponseParser\Interfaces\SfoResponseParserInterface;
use DTNL\SfoClient\ResponseParser\Exceptions\UnsupportedResponseFormatException;
use DTNL\SfoClient\Metadata\XmlMetadataResponseParser;


class SfoMetadataResponseParser implements SfoResponseParserInterface {

    /**
     * {@inheritDoc}
     * 
     * @throws UnsupportedResponseFormatException
     */
    public static function parse( ResponseInterface $response ) {

        $content_type = $response->getHeader( 'Content-Type' );
        $parts = explode( ';', $content_type[0], 2 );
        $media_type = strtolower( $parts[0] );
        
        switch ( $media_type ) {
            case 'application/atom+xml':
            case 'application/xml':
                return XmlMetadataResponseParser::parse( $response );
            default:
                throw new UnsupportedResponseFormatException();
        }
    }
}