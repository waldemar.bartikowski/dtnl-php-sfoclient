<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Metadata;

use DTNL\SfoClient\Metadata\Interfaces\SfoEntityPropertyMetadataInterface;
use DTNL\SfoClient\Metadata\Interfaces\SfoEntityMetadataInterface;
use DTNL\SfoClient\Metadata\Exceptions\SfoMetadataPropertyNotDefinedException;
use DTNL\SfoClient\Metadata\Exceptions\SfoMetadataKeyNotDefinedException;

class EntityMetadata implements SfoEntityMetadataInterface {

    /** @var string */
    protected $name;

    /** @var ?string */
    protected $namespace;

    /** @var ?string */
    protected $key;

    /** @var SfoEntityPropertyMetadataInterface[] */
    protected $properties = [];

    public function __construct( string $name, ?string $namespace = null ) {
        $this->name = $name;
        $this->namespace = $namespace;
    }


    /**
     * Get name *without* namespace.
     *
     * @return string
     */
    public function getName() : string {
        return $this->name;
    }

    /**
     * Get the namespace of this Entity.
     *
     * @return string
     */
    public function getNamespace() : string {
        if ( !isset( $this->namespace ) ) {
            return '';
        }
        return $this->namespace;
    }

    /**
     * Get the name including the namespace
     *
     * @return string
     */
    public function getFullName() : string {
        if ( $this->getNamespace() !== '' ) {
            return $this->getNamespace() . '.' . $this->getName();
        }
        return $this->getName();
    }

    /**
     * Set name of the key.
     * 
     * @var string $key
     * @return SfoEntityMetadataInterface
     */
    public function setKey( string $key ) : SfoEntityMetadataInterface {
        $this->key = $key;
        return $this;
    }

    /**
     * Get name of the key.
     * 
     * @return string
     * @throws SfoMetadataKeyNotDefinedException
     */
    public function getKey() : string {
        if ( !isset( $this->key ) ) {
            throw new SfoMetadataKeyNotDefinedException( $this );
        }
        return $this->key;
    }

    /**
     * Add property metadata.
     *
     * @param SfoEntityPropertyMetadataInterface $property
     * @return SfoEntityMetadataInterface
     */
    public function addProperty( SfoEntityPropertyMetadataInterface $property ) : SfoEntityMetadataInterface {
        $this->properties[ $property->getName() ] = $property;
        return $this;
    }

    /**
     * Get property metadata.
     *
     * @param string $name
     * @return SfoEntityPropertyMetadataInterface
     * @throws SfoMetadataPropertyNotDefinedException
     */
    public function getProperty( string $name ) : SfoEntityPropertyMetadataInterface {
        if ( !isset( $this->properties[ $name ] ) ) {
            throw new SfoMetadataPropertyNotDefinedException( $this, $name );
        }
        return $this->properties[ $name ];
    }

    /**
     * Check if property exists.
     * 
     * @return bool
     */
    public function hasProperty( string $name ) : bool {
        return isset( $this->properties[ $name ] );
    }

    /**
     * Return metadata for all properties.
     * 
     * @return SfoEntityPropertyMetadataInterface[]
     */
    public function getProperties() : array {
        return $this->properties;
    }

    public function __toString() : string {
        $string = 'Name: ' . $this->getName() . PHP_EOL;
        $string .= 'Namespace: ' . $this->getNamespace() . PHP_EOL;
        
        try {
            $string .= 'Key: ' . $this->getKey() . PHP_EOL;
        } catch( SfoMetadataKeyNotDefinedException $e ) {
            // Do nothing
        }

        return $string;
    }
}
