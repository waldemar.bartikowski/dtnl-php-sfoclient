<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Metadata;

use DTNL\SfoClient\Metadata\Interfaces\SfoMetadataRepositoryInterface;
use DTNL\SfoClient\Metadata\Interfaces\SfoEntityPropertyMetadataInterface;
use DTNL\SfoClient\Metadata\Interfaces\SfoEntityMetadataInterface;
use DTNL\SfoClient\Entity\Interfaces\SfoEntityInterface;

/**
 * @todo: Finish
 */
class MetadataEntityValidator {

    protected $metadata;

    public function __construct( SfoMetadataRepositoryInterface $metadata ) {
        $this->metadata = $metadata;
    }

    public function validate( SfoEntityInterface $entity ) : void {
        $entity_metadata = $this->getEntityMetadata( $entity );
        $this->validateProperties( $entity );
    }

    protected function validateProperties( SfoEntityInterface $entity ) : array {

        $entity_metadata = $this->getEntityMetadata( $entity );

        $errors = [];

        foreach ( $entity_metadata->getProperties() as $property_md ) {

            $property_name = $property_md->getName();
            
            if (
                !$entity->hasProperty( $property_name )
                && !$property_md->isNullable()
            ) {
                $errors[] = "Missing property " . $property_name;
            }

            if ( !$entity->hasProperty( $property_name ) ) { continue; }
            $property = $entity->getProperty( $property_name );

            if ( is_null( $property ) && !$property_md->isNullable() ) {
                $errors[] = "Property " . $property_name . " may not be NULL.";
            }

            if ( !$this->checkPropertyType( $property, $property_md ) ) {
                $errors[] =
                    'Property "' . $property_name . '" has wrong type: '
                    . gettype( $property ) . '. '
                    . ' "' . $property_md->getType() . '" expected.';
            }
        }

        return $errors;
    }

    // https://documentation.progress.com/output/DataDirect/hybridpipeline/index.html#page/hybrid/Entity_Data_Model_(EDM)_types_for_OData_Version_3.html
    private function checkPropertyType( $property, SfoEntityPropertyMetadataInterface $md ) : bool {

        switch ( $md->getType() ) {
    
            case 'Edm.Binary':
                throw new PrimitiveNotSupportedException( $type );

            case 'Edm.Boolean':
                return is_bool( $property );
            
            case 'Edm.Byte':
                return is_int( $property );
            
            case 'Edm.Date':      
            case 'Edm.DateTimeOffset':
                return ($property instanceof \DateTime );
            
            case 'Edm.Decimal':
            case 'Edm.Double':
                return is_float( $property );

            case 'Edm.Duration':
                throw new PrimitiveNotSupportedException( $type );

            case 'Edm.Guid':
                return is_string( $property );

            case 'Edm.Int16':
            case 'Edm.Int32':
            case 'Edm.Int64':
            case 'Edm.SByte':
                return is_int( $property );
                
            case 'Edm.Single':
                throw new PrimitiveNotSupportedException( $type );

            case 'Edm.Stream':
                throw new PrimitiveNotSupportedException( $type );

            case '';
            case 'Edm.String':
                return is_string( $property );

            case 'Edm.TimeOfDay':
                throw new PrimitiveNotSupportedException( $type );
            
            case 'Edm.Geography':
            case 'Edm.GeographyPoint':
            case 'Edm.GeographyLineString':
            case 'Edm.GeographyPolygon':
            case 'Edm.GeographyMultiPoint':
            case 'Edm.GeographyMultiLineString':
            case 'Edm.GeographyMultiPolygon':
            case 'Edm.GeographyCollection':
            case 'Edm.Geometry':
            case 'Edm.GeometryPoint':
            case 'Edm.GeometryLineString':
            case 'Edm.GeometryPolygon':
            case 'Edm.GeometryMultiPoint':
            case 'Edm.GeometryMultiLineString':
            case 'Edm.GeometryMultiPolygon':
            case 'Edm.GeometryCollection':
                throw new PrimitiveNotSupportedException( $type );
        }

        return true;
    }

    /**
     * @param SfoEntityInterface $entity
     * @return SfoEntityMetadataInterface
     */
    private function getEntityMetadata( SfoEntityInterface $entity ) : SfoEntityMetadataInterface {
        $entity_name = $entity->getName();
        return $this->metadata->getEntityMetadata( $entity_name );
    }
}