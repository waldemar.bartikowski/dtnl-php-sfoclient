<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Metadata;

class SapPropertyMetadata {

    /** @var bool */
    private $required = false;

    /** @var bool */
    private $creatable = false;

    /** @var bool */
    private $updatable = false;

    /** @var bool */
    private $upsertable = false;

    /** @var bool */
    private $visible = false;

    /** @var bool */
    private $sortable = false;

    /** @var bool */
    private $filterable = false;

    /** @var string */
    private $label = '';

	/**
	 * Get the value of required
	 *
	 * @return mixed
	 */
	public function getRequired() {
		return $this->required;
	}

	/**
	 * Set the value of required
	 *
	 * @param mixed $required
	 * @return self
	 */
	public function setRequired( $required ) : self {
		$this->required = $required;
		return $this;
	}

	/**
	 * Get the value of creatable
	 *
	 * @return mixed
	 */
	public function getCreatable() {
		return $this->creatable;
	}

	/**
	 * Set the value of creatable
	 *
	 * @param mixed $creatable
	 * @return self
	 */
	public function setCreatable( $creatable ) : self {
		$this->creatable = $creatable;
		return $this;
	}

	/**
	 * Get the value of updatable
	 *
	 * @return mixed
	 */
	public function getUpdatable() {
		return $this->updatable;
	}

	/**
	 * Set the value of updatable
	 *
	 * @param mixed $updatable
	 * @return self
	 */
	public function setUpdatable( $updatable ) : self {
		$this->updatable = $updatable;
		return $this;
	}

	/**
	 * Get the value of upsertable
	 *
	 * @return mixed
	 */
	public function getUpsertable() {
		return $this->upsertable;
	}

	/**
	 * Set the value of upsertable
	 *
	 * @param mixed $upsertable
	 * @return self
	 */
	public function setUpsertable( $upsertable ) : self {
		$this->upsertable = $upsertable;
		return $this;
	}

	/**
	 * Get the value of visible
	 *
	 * @return mixed
	 */
	public function getVisible() {
		return $this->visible;
	}

	/**
	 * Set the value of visible
	 *
	 * @param mixed $visible
	 * @return self
	 */
	public function setVisible( $visible ) : self {
		$this->visible = $visible;
		return $this;
	}

	/**
	 * Get the value of sortable
	 *
	 * @return mixed
	 */
	public function getSortable() {
		return $this->sortable;
	}

	/**
	 * Set the value of sortable
	 *
	 * @param mixed $sortable
	 * @return self
	 */
	public function setSortable( $sortable ) : self {
		$this->sortable = $sortable;
		return $this;
	}

	/**
	 * Get the value of filterable
	 *
	 * @return mixed
	 */
	public function getFilterable() {
		return $this->filterable;
	}

	/**
	 * Set the value of filterable
	 *
	 * @param mixed $filterable
	 * @return self
	 */
	public function setFilterable( $filterable ) : self {
		$this->filterable = $filterable;
		return $this;
	}

	/**
	 * Get the value of label
	 *
	 * @return mixed
	 */
	public function getLabel() {
		return $this->label;
	}

	/**
	 * Set the value of label
	 *
	 * @param mixed $label
	 * @return self
	 */
	public function setLabel( $label ) : self {
		$this->label = $label;
		return $this;
	}
}