<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Metadata;

use Psr\Http\Message\ResponseInterface;
use DTNL\SfoClient\ResponseParser\Interfaces\SfoResponseParserInterface;
use DTNL\SfoClient\ResponseParser\Exceptions\UnsupportedResponseTypeException;
use DTNL\SfoClient\Metadata\MetadataRepository;
use DTNL\SfoClient\Metadata\Mapper\XmlEntityMetadataMapper;
use DTNL\SfoClient\Metadata\Exceptions\InvalidMetadataException;

class XmlMetadataResponseParser implements SfoResponseParserInterface {

    /**
     * {@inheritDoc}
     * 
     * @throws UnsupportedResponseTypeException
     */
    public static function parse( ResponseInterface $response ) {
        
        $body = (string) $response->getBody();
        $xml = new \SimpleXMLElement( $body );
        $root_element_name = $xml->getName();

        if ( $root_element_name !== 'Edmx' ) {
            throw new UnsupportedResponseTypeException(
                'Unsupported response type "'
                . $root_element_name
                . '"'
            );
        }

        $schemas = $xml->children( 'edmx', true )->DataServices->children()->Schema;

        if ( $schemas->count() === 0 ) {
            throw new InvalidMetadataException(
                'No Schemas found in metadata.'
            );
        }

        $repo = new MetadataRepository();
        $repo->setDefaultNamespace( 'SFOData' );

        foreach ( $schemas as $schema ) {

            $namespace = (string) $schema->attributes()->Namespace;

            if ( $namespace === 'SFOData' ) {
                foreach ( $schema->EntityType as $entity ) {
                    $mapper = new XmlEntityMetadataMapper( $entity );
                    $meta = $mapper->map( $namespace );
                    $repo->addEntityMetadata( $meta );
                }
            }

        }

        return $repo;
    }

}