<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Metadata;

use DTNL\SfoClient\Metadata\Interfaces\SfoMetadataRepositoryInterface;
use DTNL\SfoClient\Metadata\Interfaces\SfoEntityMetadataInterface;
use DTNL\SfoClient\Metadata\Exceptions\SfoMetadataEntityNotDefinedException;

class MetadataRepository implements SfoMetadataRepositoryInterface {

    /** @var SfoEntityMetadataInterface[] */
    protected $entity_metadata;

    /** @var string|null */
    protected $default_namespace;

    /**
     * Set default namespace that is used to look for entities.
     *
     * @param ?string $namespace
     * @return SfoMetadataRepositoryInterface
     */
    public function setDefaultNamespace( ?string $namespace ) : SfoMetadataRepositoryInterface {
        $this->default_namespace = $namespace;
        return $this;
    }

    /**
     * Add metadata for an entity.
     *
     * @param SfoEntityMetadataInterface $entity_metadata
     * @return SfoMetadataRepositoryInterface
     */
    public function addEntityMetadata(
        SfoEntityMetadataInterface $entity_metadata
    ) : SfoMetadataRepositoryInterface {
       
        $this->entity_metadata[ $entity_metadata->getFullName() ] = $entity_metadata;
        return $this;
    }

    /**
     * Check if metadata for an entity exists.
     *
     * @param string $name
     * @return boolean
     */
    public function hasEntityMetadata( string $name ) : bool {
        $name = $this->prefixWithDefaultNamespace( $name );
        return isset( $this->entity_metadata[ $name ] );
    }

    /**
     * Get the metadata for an entity.
     *
     * @param string $name
     * @return SfoEntityMetadataInterface
     * @throws \DTNL\SfoClient\Metadata\Exceptions\SfoMetadataEntityNotDefinedException
     */
    public function getEntityMetadata( string $name ) : SfoEntityMetadataInterface {
        $name = $this->prefixWithDefaultNamespace( $name );
        if ( !$this->hasEntityMetadata( $name ) ) {
            throw new SfoMetadataEntityNotDefinedException( $name );
        }
        return $this->entity_metadata[ $name ];
    }


    /**
     * Get metadata for all entities.
     *
     * @return SfoEntityMetadataInterface[]
     */
    public function getAllEntityMetadata() : array {
        return $this->entity_metadata;
    }

    private function prefixWithDefaultNamespace( string $name ) : string {
        $name = $this->stripDefaultNamespace( $name );
        if ( isset( $this->default_namespace )
          && $this->default_namespace !== '') {
            return $this->default_namespace . '.' . $name;
        }
        return $name;
    }

    private function stripDefaultNamespace( string $name ) : string {
        if ( strpos( $name, $this->default_namespace . '.' ) !== 0 ) {
            return $name;
        }
        return str_replace( $this->default_namespace . '.', '', $name );
    }
}