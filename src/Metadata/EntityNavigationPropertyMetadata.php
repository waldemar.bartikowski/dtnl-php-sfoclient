<?php
declare( strict_types = 1 );
namespace DTNL\SfoClient\Metadata;

use DTNL\SfoClient\Metadata\Interfaces\SfoEntityPropertyMetadataInterface;
use DTNL\SfoClient\Metadata\EntityPropertyMetadata;

class EntityNavigationPropertyMetadata
    extends EntityPropertyMetadata
    implements SfoEntityPropertyMetadataInterface
{
    /** @var string */
    protected $fromRole;

    /** @var string */
    protected $toRole;

    /** @var string */
    protected $relationship;

    public function __construct(
        string $name,
        string $type,
        bool $nullable,
        string $fromRole,
        string $toRole,
        string $relationship
    ) {
        parent::__construct( $name, $type, $nullable );
        $this->fromRole = $fromRole;
        $this->toRole = $toRole;
        $this->relationship = $relationship;
    }


	/**
	 * Get the value of fromRole
	 *
	 * @return string
	 */
	public function getFromRole() : string {
		return $this->fromRole;
	}

	/**
	 * @return string
	 */
	public function getToRole() : string {
		return $this->toRole;
	}

	/**
	 * @return string
	 */
	public function getRelationship() : string {
		return $this->relationship;
	}
}